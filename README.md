# BBB-Streaming Demo Setup

BBB-Streaming Demo Setup using Docker


## Setup

* Start conatiners

```
docker-compose up -d --build
```

* Create Admin User

```
docker-compose exec greenlight bundle exec rake "admin:create[Jane Doe,jane.doe@example.org,changeme,admin]"
```

* Django user grant

```
docker-compose exec django_web ./manage.py shell
```

```python
from django.contrib.auth.models import User
u = User.objects.get(username='jane.doe@example.org')
u.is_staff=True
u.is_superuser=True
u.save()
```

* Django: `http://localhost/` and `http://localhost/admin/`
* Greenlight: `http://localhost/b`
* Janus: `http://localhost/janus-api` and `http://localhost/janus-ws` (WebSocket)

* Add Janus-Server in Django-Admin
    * Rest: http://192.168.42.1/janus-api
    * WS: ws://192.168.42.1/janus-ws
    * RTP: 192.168.42.1
    * Adminkey: supersecret
        * Adminkey is set up in `janus/config/janus.plugin.streaming.jcfg`


## Caveats

* Janus conatiner is bound to the host network
* Nginx port is mapped to port 80 localhost

### Host Networtking

Janus is traffic intense. Increase the receive socket buffer size (bytes). Check for errors with `netstat -su`.

```
sysctl -w net.core.rmem_default=26214400
sysctl -w net.core.rmem_max=26214400
```

### Nginx Performance Tuning

We may have multiple hundret of connections going through nginx. Add some performance tuning:

```
events {
	worker_connections 10000;
	# multi_accept on;
}

http {
    # cache informations about FDs, frequently accessed files
    # can boost performance, but you need to test those values
    open_file_cache max=200000 inactive=20s;
    open_file_cache_valid 30s;
    open_file_cache_min_uses 2;
    open_file_cache_errors on;

    # to boost I/O on HDD we can disable access logs
    #access_log off;

    # gzip_static on;
    gzip_min_length 10240;
    gzip_comp_level 1;
    gzip_vary on;
    gzip_disable msie6;
    gzip_proxied expired no-cache no-store private auth;
    gzip_types
        # text/html is always compressed by HttpGzipModule
        text/css
        text/javascript
        text/xml
        text/plain
        text/x-component
        application/javascript
        application/x-javascript
        application/json
        application/xml
        application/rss+xml
        application/atom+xml
        font/truetype
        font/opentype
        application/vnd.ms-fontobject
        image/svg+xml;

    # allow the server to close connection on non responding client, this will free up memory
    reset_timedout_connection on;

    # request timed out -- default 60
    client_body_timeout 10;

    # if client stop responding, free up memory -- default 60
    send_timeout 2;

    # server will close connection after this time -- default 75
    keepalive_timeout 30;

    # number of requests client can make over keep-alive -- for testing environment
    keepalive_requests 100000;
}
```