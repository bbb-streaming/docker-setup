# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "generic/debian10"

  config.vm.define "janus" do |janus|
    janus.vm.hostname = "janus"
    janus.vm.network :private_network, ip: "192.168.142.10"
    
    janus.vm.synced_folder "./", "/opt/bbb_streaming", type: "rsync",
      rsync__exclude: ".git/"

    janus.vm.provision "shell", inline: <<-SHELL
      DEBIAN_FRONTEND=noninteractive apt install -y docker-compose nginx
      cd /opt/bbb_streaming
      unlink /etc/nginx/sites-enabled/default || true
      cp nginx.janus.conf /etc/nginx/sites-available/janus
      ln -s /etc/nginx/sites-available/janus /etc/nginx/sites-enabled/janus
      
      if ! [ -f "/etc/ssl/ssl-dhparams.pem" ]; then
        curl https://ssl-config.mozilla.org/ffdhe2048.txt > /etc/ssl/ssl-dhparams.pem
        openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/janus.key -out /etc/ssl/certs/janus.crt -subj '/CN=janus'
      fi

      systemctl restart nginx
      docker-compose -f docker-compose.janus.yml down --remove-orphans
      docker-compose -f docker-compose.janus.yml up --build -d
    SHELL
  end

  config.vm.define "django" do |django|
    django.vm.hostname = "django"
    django.vm.network :private_network, ip: "192.168.142.11"
    
    django.vm.synced_folder "./", "/opt/bbb_streaming", type: "rsync",
      rsync__exclude: ".git/"

    django.vm.provision "shell", inline: <<-SHELL
      DEBIAN_FRONTEND=noninteractive apt install -y docker-compose nginx
      cd /opt/bbb_streaming
      unlink /etc/nginx/sites-enabled/default || true
      cp nginx.web.conf /etc/nginx/sites-available/web
      ln -s /etc/nginx/sites-available/web /etc/nginx/sites-enabled/web

      if ! [ -f "/etc/ssl/ssl-dhparams.pem" ]; then
        curl https://ssl-config.mozilla.org/ffdhe2048.txt > /etc/ssl/ssl-dhparams.pem
        openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/web.key -out /etc/ssl/certs/web.crt -subj '/CN=web'
      fi

      systemctl restart nginx
      docker-compose -f docker-compose.web.yml down --remove-orphans
      docker-compose -f docker-compose.web.yml up --build -d
    SHELL
  end

  config.vm.define "worker" do |worker|
    worker.vm.hostname = "worker"
    worker.vm.network :private_network, ip: "192.168.142.12"

    worker.vm.provision "shell", inline: <<-SHELL
      DEBIAN_FRONTEND=noninteractive apt install -y docker-compose
      cd /opt/
      git clone https://gitlab.com/bbb-streaming/bbb-rtp-streamer
      cd bbb-rtp-streamer
      docker build -t bbb-rtp-streamer .
      docker stop bbb-rtp-streamer
      while ! nc -z 192.168.142.11 5672; do
        sleep 1
      done
      docker run --rm -e WORKER_ID=11082720-d945-47b3-b9dc-03700b8dac31 -e AMQP_URI="amqp://bbb_streaming:changeme@192.168.142.11:5672/bbb_streaming" --name bbb-rtp-streamer -d bbb-rtp-streamer
    SHELL
  end
end
