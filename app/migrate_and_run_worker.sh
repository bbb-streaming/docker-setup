#!/bin/sh

if [ "$WEB_SQL_ENGINE" = "django.db.backends.postgresql" ]
then
    if ! nc -z $WEB_SQL_HOST $WEB_SQL_PORT; then
      echo "PostgreSQL not available"
      exit 1
    fi
    echo "PostgreSQL available"
fi

if ! nc -z $RABBIT_HOST 5672; then
    echo "RabbitMQ not available"
    exit 1
fi

echo "RabbitMQ available"

./manage.py migrate --noinput
./manage.py collectstatic --noinput --clear

exec ./manage.py mq_worker
