"""
Django settings for app project.

Generated by 'django-admin startproject' using Django 3.2.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.2/ref/settings/
"""

from pathlib import Path
import os
import pika

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get("SECRET_KEY", "django-insecure-wfcuf55_i0z4quxw16ng@7qc41rl4ek5snv^1n^f_g&hr&%q2y")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.environ.get("DEBUG", "false").lower() == "true"

ALLOWED_HOSTS = os.environ.get("ALLOWED_HOSTS", "localhost 127.0.0.1 [::1]").split(' ')


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'bbb_streaming',
    'crispy_forms',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'app.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'app.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": os.environ.get("WEB_SQL_ENGINE", "django.db.backends.sqlite3"),
        "NAME": os.environ.get("WEB_SQL_DATABASE", os.path.join(BASE_DIR, "db_web.sqlite3")),
        "USER": os.environ.get("WEB_SQL_USER", "user"),
        "PASSWORD": os.environ.get("WEB_SQL_PASSWORD", "password"),
        "HOST": os.environ.get("WEB_SQL_HOST", "localhost"),
        "PORT": os.environ.get("WEB_SQL_PORT", "5432"),
    },
    "greenlight": {
        "ENGINE": os.environ.get("GREENLIGHT_SQL_ENGINE", "django.db.backends.sqlite3"),
        "NAME": os.environ.get("GREENLIGHT_SQL_DATABASE", os.path.join(BASE_DIR, "db_greenlight.sqlite3")),
        "USER": os.environ.get("GREENLIGHT_SQL_USER", "user"),
        "PASSWORD": os.environ.get("GREENLIGHT_SQL_PASSWORD", "password"),
        "HOST": os.environ.get("GREENLIGHT_SQL_HOST", "localhost"),
        "PORT": os.environ.get("GREENLIGHT_SQL_PORT", "5432"),
    }
}


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static")

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(asctime)s %(levelname)s %(module)s: "%(message)s"',
        },
    },
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
        }
    },
    'root': {
        'handlers': ['mail_admins', 'console'],
        'level': 'INFO',
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
        },
        'django.request': {
            'handlers': ['mail_admins', 'console'],
            'level': 'ERROR',
            'propagate': False,
        },
        'django.security': {
            'handlers': ['mail_admins', 'console'],
            'level': 'WARNING',
            'propagate': False,
        },
        'pika': {
            'handlers': ['console'],
            'level': 'WARNING',
        },
    }
}


BBB_PROVIDER = os.environ.get("BBB_PROVIDER", "bbb_streaming.bbb_provider.GreenlightProvider")
AUTHENTICATION_BACKENDS = ['bbb_streaming.auth_backend.GreenlightAuthBackend']

RABBIT_CONNECTION = {
    'host': os.environ.get("RABBIT_HOST", "localhost"),
    'virtual_host': os.environ.get("RABBIT_VHOST", ""),
    'credentials': pika.PlainCredentials(os.environ.get("RABBIT_USER", "user"), os.environ.get("RABBIT_PASS", "changeme")),
}

RABBIT_WORKER_QUEUE = os.environ.get("RABBIT_WORKER_QUEUE", "jobs")
RABBIT_RECEIVE_QUEUE = os.environ.get("RABBIT_RECEIVE_QUEUE", "controller")

BBB_API_ENDPOINT = os.environ.get("BBB_API_ENDPOINT", "")
BBB_API_SECRET = os.environ.get("BBB_API_SECRET", "")

JANUS_MEDIA_PORT_START = int(os.environ.get("JANUS_MEDIA_PORT_START", "5000"))
JANUS_MEDIA_PORT_STEP = 10

GREENLIGHT_SOCAL_DOMAIN = os.environ.get("GREENLIGHT_SOCAL_DOMAIN", "example.org")
BBB_JOIN_URL = os.environ.get("BBB_JOIN_URL", "https://example.org/gl")

TURN_SECRET = os.environ.get("TURN_SECRET", "changeme")
TURN_URI = os.environ.get("TURN_URI", "")
STUN_URI = os.environ.get("STUN_URI", "")

if os.environ.get("JANUS_LB_REST_URL", None) is not None:
    JANUS_LB_REST_URL = os.environ["JANUS_LB_REST_URL"]
if os.environ.get("JANUS_LB_WSS_URL", None) is not None:
    JANUS_LB_WSS_URL = os.environ["JANUS_LB_WSS_URL"]
