# Multi Host Setup (Manual)

## Janus

```
sudo -s
apt install docker-compose nginx
cd /opt
git clone https://gitlab.com/bbb-streaming/docker-setup bbb_streaming
cd bbb_streaming
```

Adopt config:

* `/opt/bbb_streaming/janus/config/janus.jcfg` → change `adminsecret`
* `/opt/bbb_streaming/janus/config/janus.plugin.streaming.jcfg` → change `admin_key`
* `/opt/bbb_streaming/janus/config/janus.transport.http.jcfg`
* `/opt/bbb_streaming/janus/config/janus.transport.websockets.jcfg`

Add nginx config

```
unlink /etc/nginx/sites-enabled/default
cp nginx.janus.conf /etc/nginx/sites-available/janus
ln -s /etc/nginx/sites-available/janus /etc/nginx/sites-enabled/janus
systemctl restart nginx
```

Start Docker:

```
docker-compose -f docker-compose.janus.yml up --build -d
```

## Django, Greenlight, Rabbit

```
sudo -s
apt install docker-compose nginx
cd /opt
git clone https://gitlab.com/bbb-streaming/docker-setup bbb_streaming
cd bbb_streaming
```

Adopt config:

* `django.env`
* `rabbit.env`
* `greenlight.env`

Add nginx config

```
unlink /etc/nginx/sites-enabled/default
cp nginx.web.conf /etc/nginx/sites-available/web
ln -s /etc/nginx/sites-available/web /etc/nginx/sites-enabled/web
systemctl restart nginx
```

Start Docker:

```
docker-compose -f docker-compose.web.yml up --build -d
docker-compose -f docker-compose.web.yml exec greenlight bundle exec rake "admin:create[Jane Doe,jane.doe@example.org,changeme,admin]"
```

login to the Django admin interface using the jane doe user: https://your-contoller/admin -> this will fail but create a user in Django.
Next grant this user admin permissions in Django:

```
docker-compose -f docker-compose.web.yml exec django_web ./manage.py shell --command="from django.contrib.auth.models import User;u = User.objects.get(username='jane.doe@example.org');u.is_staff=True;u.is_superuser=True;u.save()"
```

* Login to Django admin: https://your-controller/admin
* add each Janus server as Streaming server in Django admin
  * Rest endpoint: https://janus-server/janus-api
  * Ws Endpoint: wss://janus-server/janus-ws
  * RTP IP: IP address of your janus server
  * secret: the secret that you entered in `janus.plugin.streaming.jcfg`



Run without Greenlight:

```
docker-compose -f docker-compose.django-rabbit.yml up --build -d
```
